(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(require 'init-elpa)
(require 'init-ui)
(require 'init-editing)
(require 'init-navigation)
(require 'init-misc)
(require 'init-company-mode)
(require 'init-markdown)
(require 'init-rust)
(require 'init-yaml)
(require 'init-javascript)
(require 'init-haskell)
(require 'init-scala)
(require 'init-magit)
(require 'init-mmm)
(require 'init-org)
(require 'unbound)

(provide 'init)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(highlight-indent-guides-auto-character-face-perc 20)
 '(highlight-indent-guides-delay 0)
 '(highlight-indent-guides-method 'character)
 '(js-indent-level 2)
 '(lsp-rust-analyzer-cargo-watch-command "clippy")
 '(markdown-fontify-code-blocks-natively t)
 '(org-agenda-files nil)
 '(org-format-latex-header
   "\\documentclass{article}
\\usepackage[usenames]{color}
[PACKAGES]
[DEFAULT-PACKAGES]
\\pagestyle{empty}             % do not remove
\\setlength{\\textwidth}{\\paperwidth}
\\addtolength{\\textwidth}{-3cm}
\\setlength{\\oddsidemargin}{1.5cm}
\\addtolength{\\oddsidemargin}{-2.54cm}
\\setlength{\\evensidemargin}{\\oddsidemargin}
\\setlength{\\textheight}{\\paperheight}
\\addtolength{\\textheight}{-\\headheight}
\\addtolength{\\textheight}{-\\headsep}
\\addtolength{\\textheight}{-\\footskip}
\\addtolength{\\textheight}{-3cm}
\\setlength{\\topmargin}{1.5cm}
\\addtolength{\\topmargin}{-2.54cm}
\\usepackage[margin=2cm]{geometry}
\\setlength{\\parindent}{0pt}
\\setlength{\\parskip}{\\baselineskip}")
 '(package-selected-packages
   '(fish-mode auctex cdlatex scala-mode s pcache list-utils git-commit flycheck f dash bind-key async gitattributes-mode yaml-mode xclip use-package unicode-whitespace tide smex rainbow-delimiters projectile prettier-js poporg mmm-mode markdown-mode magit js2-mode ido-completing-read+ highlight-indent-guides haskell-mode flycheck-rust expand-region dot-mode company centered-cursor-mode atom-one-dark-theme))
 '(prettier-js-show-errors nil)
 '(rustic-format-trigger nil)
 '(rustic-rustfmt-bin "rustfmt")
 '(scroll-conservatively 9999)
 '(scroll-margin 999)
 '(scroll-preserve-screen-position 1)
 '(tide-format-options '(indentSize 2 tabSize 2)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background "black")))))
