(require 'init-elpa)

(use-package lsp-mode
  :ensure t
  :init (setq lsp-lens-enable nil
              lsp-signature-auto-activate nil
              lsp-signature-doc-lines 1))
(require 'lsp-mode)

(use-package lsp-ui
  :ensure t
  :after (lsp-mode)
  :init (setq lsp-ui-doc-enable nil
              lsp-ui-peek-enable t
              lsp-ui-peek-show-directory t
              lsp-ui-sideline-update-mode 'line
              lsp-ui-sideline-enable t
              lsp-ui-sideline-show-code-actions t
              lsp-ui-sideline-show-hover nil
              lsp-ui-sideline-ignore-duplicate t)
  
  ;; `C-g'to close doc
  (advice-add #'keyboard-quit :before #'lsp-ui-doc-hide)

  ;; Reset `lsp-ui-doc-background' after loading theme
  (add-hook 'after-load-theme-hook
            (lambda ()
              (setq lsp-ui-doc-border (face-foreground 'default))
              (set-face-background 'lsp-ui-doc-background
                                   (face-background 'tooltip))))

  ;; WORKAROUND Hide mode-line of the lsp-ui-imenu buffer
  ;; @see https://github.com/emacs-lsp/lsp-ui/issues/243
  (defadvice lsp-ui-imenu (after hide-lsp-ui-imenu-mode-line activate)
    (setq mode-line-format nil)))

(require 'lsp-ui)

;; (use-package rustic :ensure t)
;; (require 'rustic)
;; (setq rustic-lsp-server 'rust-analyzer)
;; (setq rustic-analyzer-command
;;       '("~/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin/rust-analyzer"))

;; (defun find-rustic ()
;;   (when
;;     (string= (file-name-extension buffer-file-name) "rs")
;;     (rustic-mode)))
;; (add-hook 'find-file-hook 'find-rustic)

(defun custom-rust-setup ()
  (electric-pair-mode 1)
   ;; (setq rustic-format-on-save t)
  ;; (defun custom-rust-lsp-configure () (
  ;;     (lsp-rust-analyzer-inlay-hints-mode 1)))
  ;; (add-hook 'lsp-rust-analyzer-after-open-hook 'custom-rust-lsp-configure))
  )
;; (add-hook 'rustic-mode-hook 'custom-rust-setup)

(setq lsp-headerline-breadcrumb-enable nil)

(require 'rust-rustfmt)

(provide 'init-rust)
