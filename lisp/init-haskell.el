(require 'init-elpa)
(require 'init-editing)
(require 'init-company-mode)

(use-package haskell-mode :ensure t)
(require 'haskell-mode)

(add-to-list 'auto-mode-alist '("\\.hs\\'" . haskell-mode))
(add-hook 'haskell-mode-hook '(lambda () (company-mode 1) (flycheck-mode 1)))

(provide 'init-haskell)
