(require 'init-elpa)

(defun custom-org-setup ()
  (org-indent-mode 1)
  (visual-line-mode 1))
  
(add-hook 'org-mode-hook 'custom-org-setup)

(font-lock-add-keywords 'org-mode
  '(("^ +\\([-*]\\) "
     (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "∙"))))))

;; (use-package auctex :ensure t)
;; (use-package cdlatex :ensure t)

;; (add-hook 'LaTeX-mode-hook 'turn-on-cdlatex)
;; (add-hook 'org-mode-hook 'turn-on-org-cdlatex)

(require 'ox-latex)

(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("frame" "lines") ("linenos=true")))

(setq org-latex-pdf-process
      (mapcar
       (lambda (s)
         (replace-regexp-in-string "%latex " "%latex -shell-escape " s))
       org-latex-pdf-process))

(provide 'init-org)
