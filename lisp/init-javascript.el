(require 'init-elpa)
(require 'init-editing)

(use-package typescript-mode :ensure t)
(use-package js2-mode :ensure t)

(add-to-list 'auto-mode-alist '("\\.[jt]sx?\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.[jt]sx?\\'" . typescript-mode))

(setq read-process-output-max (* 1024 1024))

(setq gc-cons-threshold 100000000)

(defgroup eslint-fix nil
  "Fix JavaScript linting issues with ‘eslint-fix’."
  :link '(function-link eslint-fix)
  :tag "ESLint Fix"
  :group 'tools)

(defcustom eslint-fix-executable "eslint"
  "The ESLint executable to use."
  :tag "ESLint Executable"
  :type 'string)

(defcustom eslint-fix-options nil
  "Additional options to pass to ESLint (e.g. “--quiet”)."
  :tag "ESLint Options"
  :type '(repeat string))

;;;###autoload
(defun eslint-fix ()
  "Format the current file with ESLint."
  (interactive)
  (unless buffer-file-name
    (error "ESLint requires a file-visiting buffer"))
  (when (buffer-modified-p)
    (if (y-or-n-p (format "Save file %s? " buffer-file-name))
        (save-buffer)
      (error "ESLint may only be run on an unmodified buffer")))

  (let ((eslint (executable-find eslint-fix-executable))
        (options (append eslint-fix-options
                         (list "--fix" buffer-file-name))))
    (unless eslint
      (error "Executable ‘%s’ not found" eslint-fix-executable))
    (apply #'call-process eslint nil "*ESLint Errors*" nil options)
    (revert-buffer t t t)))

(use-package tide
  :ensure t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)))

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (prettier-js-mode +1)
  (electric-pair-mode +1)
  (company-mode +1))

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

(add-hook 'typescript-mode-hook #'setup-tide-mode)

(add-hook 'js2-mode-hook #'setup-tide-mode)

;; (flycheck-add-next-checker 'javascript-eslint 'javascript-tide 'append)

;; (eval-after-load 'js2-mode
;; 	   '(add-hook 'js2-mode-hook (lambda () (add-hook 'after-save-hook 'eslint-fix nil t))))

(use-package prettier-js :ensure t)
(require 'prettier-js)

;; (add-hook 'js2-mode-hook 'prettier-js-mode)
;; (add-hook 'js2-mode-hook 'electric-pair-mode)

(provide 'init-javascript)
