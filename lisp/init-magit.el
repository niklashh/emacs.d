(require 'init-elpa)
(require 'use-package)

(use-package magit :ensure t)

(provide 'init-magit)
