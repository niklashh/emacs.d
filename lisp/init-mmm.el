(require 'init-elpa)

(use-package mmm-mode :ensure t)

;; (setq mmm-global-mode 'maybe)
;; (require 'mmm-mode)
;; (mmm-add-classes
;; '((markdown-rust
;; :submode rustic-mode
;; :face mmm-declaration-submode-face
;; :front "```(?:rust)?"
;; :back "```")))

;; (mmm-add-mode-ext-class 'markdown-mode nil 'markdown-rust)

;; (mmm-add-classes
;; '((org-rust
;; :submode rustic-mode
;; :face mmm-declaration-submode-face
;; :front "#\\+begin_src rust[^\n]*\n"
;; :back "\n\s*#\\+end_src")))

;; (mmm-add-mode-ext-class 'org-mode nil 'org-rust)
;; (add-hook 'org-mode-hook 'mmm-mode)

(setq mmm-parse-when-idle 't)

(provide 'init-mmm)
