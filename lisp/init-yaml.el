(require 'init-elpa)
(require 'init-editing)

(use-package "yaml-mode" :ensure t)

(require 'yaml-mode)

(add-to-list 'auto-mode-alist '("\\.ya?ml\\'" . yaml-mode))

(provide 'init-yaml)
