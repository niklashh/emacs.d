(require 'init-elpa)

(use-package company :ensure t)
(require 'company)

(setq company-dabbrev-downcase 0)
(setq company-idle-delay 0)

(setq company-tooltip-align-annotations t)
(add-hook 'prog-mode-hook 'company-mode)

(provide 'init-company-mode)
