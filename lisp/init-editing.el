(require 'init-elpa)

(setenv "PATH" (concat (getenv "PATH") ":~/.local/bin"))
(setq exec-path (append exec-path '("~/.local/bin")))

(use-package rainbow-delimiters :ensure t)
(use-package flycheck :ensure t)

(save-place-mode 1)

;; Highlights matching parenthesis
(show-paren-mode 1)

;; Highlight current line
(global-hl-line-mode 1)

;; Interactive search key bindings. By default, C-s runs
;; isearch-forward, so this swaps the bindings.
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

(setq x-select-enable-clipboard t)

;; When you visit a file, point goes to the last place where it
;; was when you previously visited the same file.
;; http://www.emacswiki.org/emacs/SavePlace

(setq-default save-place t)

;; keep track of saved places in ~/.emacs.d/places
(setq save-place-file (concat user-emacs-directory "places"))

;; Emacs can automatically create backup files. This tells Emacs to
;; put all backups in ~/.emacs.d/backups. More info:
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))
(setq auto-save-default nil)
(setq make-backup-files nil)

(defun toggle-comment-on-line ()
  "Comment or uncomment current line."
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))
(global-set-key (kbd "C-;") 'toggle-comment-on-line)

(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(defvar my-keys-minor-mode-map
  (let ((map (make-sparse-keymap)))
      (global-set-key (kbd "M->") 'indent-rigidly-right-to-tab-stop)
      (global-set-key (kbd "M-<") 'indent-rigidly-left-to-tab-stop)
    map)
  "my-keys-minor-mode keymap.")

(define-minor-mode my-keys-minor-mode
  "Override keys."
  :init-value t
  :lighter " 🥔")

(my-keys-minor-mode 1)

(setq-default tab-width 2)
(setq-default indent-tabs-mode nil)
(setq c-set-style "k&r")
(setq c-basic-offset 2)
(setq tab-stop-list (number-sequence 2 120 2))
(setq-default indent-tabs-mode nil)

(use-package highlight-indent-guides :ensure t)

(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)

(use-package xclip :ensure t)
(require 'xclip)
(xclip-mode 1)

(use-package expand-region :ensure t)
(require 'expand-region)
(global-set-key (kbd "<up>") 'er/expand-region)
(global-set-key (kbd "<down>") 'er/contract-region)

(use-package dot-mode :ensure t)
(require 'dot-mode)
(add-hook 'find-file-hooks 'dot-mode-on)

(global-set-key (kbd "C-o") 'dot-mode-execute)

(use-package gitattributes-mode :ensure t)

(use-package ws-butler :ensure t :config (add-hook 'prog-mode-hook #'ws-butler-mode))

(provide 'init-editing)
