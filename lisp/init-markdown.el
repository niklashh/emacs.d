(require 'init-elpa)

(use-package markdown-mode :ensure t)

(add-to-list 'auto-mode-alist '("\\.md\\'" . gfm-mode))

(provide 'init-markdown)
