(require 'init-elpa)

(use-package scala-mode :ensure t)

(provide 'init-scala)
